﻿# Coloca el código de tu juego en este archivo.

# Declara los personajes usados en el juego como en el ejemplo:

define sara = Character("Sara")
define benja = Character("Benjamin")
define hip = Character("Hipolito")
define nar = Character ("Narrador")
define hom = Character ("Hombre misterioso")
define adv = Character ("Advertencia")
define syb = Character ("Sara y Benja")

 
image mariposas animated:
    "marip1"
    pause .5
    "marip2"
    pause .5
    repeat
    

image lluvia animated:
    "lluvia_1"
    pause .5
    "lluvia_2"
    pause .5
    repeat
    

# El juego comienza aquí.

label start:

    play music "audio/tomates.mp3" fadein 1.0 loop
    show ciudad with zoomin
    nar "Esta historia comenzó un día de lluvia, cuando la ciudad estaba colapsada. En la puerta de nuestra casa apareció una bolita blanca de plumas y pelos."
    show lluvia animated with Dissolve(10.0)
    ###################################################################
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara " Benjamin mira lo que encontre."
    ###################################################################
    show hipolito bebe with hpunch:
        xzoom 0.5 yzoom 0.5
        yalign 0.75
        xalign 0.5
    ###################################################################        
    hide sara
    show benja with dissolve:
        yalign 0.62
        xalign 0.1
    benja "¿Es un gatito?"
    ###################################################################
    hide benja
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara "No se ¿capaz un perrito?"
    ###################################################################
    hide sara
    show benja with dissolve:
        yalign 0.62
        xalign 0.1
    benja "Pero tiene alas ¿Es un animal mitológico?"
    benja "¿Qué hacemos?"
    hide benja
    hide hipolito bebe
    menu:
        "Seguimos con nuestra vida.":
            "Te has perdido la oportunidad de conocer una historia llena de aventuras."
            jump fin_negativo
        " Lo adoptemos.":
            ############################################################   
            show sara with dissolve:
                yalign 0.62
                xalign 0.9
            sara "Me parece una excelente idea"
    ###################################################################
    hide sara
    show benja with dissolve:
        yalign 0.62
        xalign 0.1
    benja "¿Cómo lo nombramos?"
    ###################################################################
    hide benja
    label nombre:
        hide sara
        hide benja
        hide hipolito
        menu:
            "Falkor":
                show sara with dissolve:
                    yalign 0.62
                    xalign 0.9
                sara "Se parece a Falkor"
                sara "Falkor es un dragon blanco de la suerte,"
                sara "uno de los personjas mas conocidos de la novela La historia sin fin de Michael Ende"
                ###################################################################
                hide sara
                show benja with dissolve:
                    yalign 0.62
                    xalign 0.1
                benja "Me encanta ese libro.  Pero no, no es un nombre adecuado"
                ###################################################################
                jump nombre
            "Adriano":
                show sara with dissolve:
                    yalign 0.62
                    xalign 0.9
                sara "¿¿Mmm Adriano??"
                sara "Adriano fue un emperador romano conocido por gobernar con justicia y promover la paz. "
                
                ###################################################################
                hide sara
                show benja with dissolve:
                    yalign 0.62
                    xalign 0.1
                benja "No, no me gusta"
                ###################################################################
                hide benja
                jump nombre
            "Hipolito":
                show sara with dissolve:
                    yalign 0.62
                    xalign 0.9
                sara "Hipólito, el perro dragón"
                ###################################################################
                hide sara
                show benja with dissolve:
                    yalign 0.62
                    xalign 0.1
                benja "Siiii, mi perro-dragón"
                ###################################################################
                hide benja
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara "Es un nombre adecuado para un perro-dragón de alas blancas con destellos dorados, patas grandes y pesadas para aterrizar de grandes vuelos. La cara cansada de viejito sabio y las ganas de un niño chiquito"
    ###################################################################
    scene living 1 with vpunch
    show hipolito sentado:
        parallel:
            linear 1.0 xalign 0.0
            linear 1.0 xalign 1.0
            repeat
        parallel:
            linear 1.3 yalign 1.0
            linear 1.3 yalign 0.0
            repeat
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara "Nuestra casa es una pista de aterrizaje porque Hipolito esta aprendiendo a volar"
    ###################################################################
    hide sara
    hide hipolito sentado
    show hipolito sentado with wipeleft
    show mariposas animated:
        yalign 0.1
        xalign 0.5
    show benja with dissolve:
        yalign 0.62
        xalign 0.1
    benja "También hay mariposas azules por todos lados que le dan besitos en la nariz"
    ###################################################################
    hide benja
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara " Hipólito tiene una vieja cicatriz de tres puntas cerca del cuello, entre la cabeza y las alas"
    sara "Con mi hermano decidimos averiguar su historia"
    sara "¿Dónde podemos buscar información?"
    label menu_1:
    #show logos:
    #    xzoom 0.2 yzoom 0.2
    #    yalign 0.32
    #    xalign 0.66
    menu:
        "Preguntar a una vecina":
            #hide screen oraculo
            jump vecina

        "Biblioteca":
            #hide screen oraculo
            jump biblioteca

        "Internet":
            #hide screen oraculo
            jump internet

    label vecina:
        show sara with dissolve:
            yalign 0.62
            xalign 0.9
        sara "No encontramos a la vecina en su casa"
        hide sara
        jump menu_1

    label biblioteca:
        scene living 1
        show libro 1:
            yalign 0.6
            xalign 0.5
            zoom 2
        show sara with dissolve:
            yalign 0.62
            xalign 0.9
        sara "En un viejo libro que tenía por título Grimorio de animales fantásticos, descubrimos que las marcas de ese tipo podían ser por dos razones:"
        sara "Para saber más podemos mirar en el mapa."
    label ver_mapa:  
        scene living 1
        menu:
            "Ver en el mapa":
                jump mapa
            "Seguir leyendo":
                jump living_roto
    label living_roto:
        scene living roto
        show libro 2:
            yalign 0.6
            xalign 0.5
            zoom 2
        show sara with dissolve:
            yalign 0.62
            xalign 0.9
        sara "Hicimos los deberes y cuando volvimos a la cocina…"
        sara "Hipólito se había comido todoooo el libro y también nuestra merienda"
        sara "¡Los perros dragones se comen todo lo que encuentran!"
        sara "¿Qué hacemos?"
        menu:
            "Armar el libro":
                jump armar_libro
            "volver a la biblioteca":
                hide sara
                show benja with dissolve:
                    yalign 0.62
                    xalign 0.1
                benja "Vamos a explicarle a la bibliotecaria"
                hide benja
                show sara with dissolve:
                    yalign 0.62
                    xalign 0.9
                sara "Está bien, seguiremos buscando en otro libro."
        scene callejon
        show todes with moveinright
        sara "decidimos salir a la calle para buscar las respuestas"
        hide sara
        show benja with dissolve:
            yalign 0.62
            xalign 0.1
        benja "Creo que alguien nos sigue. Escucho unos pasos muy cerca."
        hide benja
    label callejon:
        scene callejon personas
        play music "audio/organo.mp3"
        sara "- Hola" 
        benja "- ¿Por qué nos sigue?"
        scene callejon
        show hombre with dissolve:
            yalign 0.5
            xalign 0.5
        hom "Yo sé dónde están las respuestas que buscan."
        show benja with dissolve:
            yalign 0.62
            xalign 0.9
        benja "Disculpe señor pero es una respuesta extraña"
        hide benja
        hom "Yo sé todo, conozco todo."
        hom "Vayan a esta dirección:  Calle del Sol 456."
        menu:
            "Seguir las instrucciones del señor.":
                jump trampa
            "Volver a casa.":
                show sara with dissolve:
                    yalign 0.62
                    xalign 0.9
                play music "audio/tomates.mp3"

                sara "Alertados por las señales de Hipólito, seguimos caminando despacito por las calles de la ciudad y no fuimos hacia la dirección indicada." 
                sara "Decidimos volver a casa y armar el libro. "
                hide sara
                jump armar_libro


    label internet:
        show sara with dissolve:
            yalign 0.62
            xalign 0.9
        sara "En internet no, porque Hipólito es un  animal ancestral y esas historias ya quedaron olvidadas en algún libro viejo."
        hide sara
        jump menu_1
    
    label salida:
        "salida"
    label trampa:
        scene libreria
        play music "audio/tomates.mp3"
        show sara with dissolve:
            yalign 0.62
            xalign 0.9
        sara "Busquemos la dirección."
        hide sara
        show benja with dissolve:
            yalign 0.62
            xalign 0.1
        benja "Mira, una librería de artes mágicas."
        hide benja
        show sara with dissolve:
            yalign 0.62
            xalign 0.9
        sara "¿Sera la dirección correcta?"
        hide sara
        show benja with dissolve:
            yalign 0.62
            xalign 0.1
        benja "Entremos a corroborar."
        hide benja
        scene negro with wipeup
        play music "audio/organo.mp3"
        " "
        show benja sorprendido with vpunch:
            xzoom 0.5 yzoom 0.5
            yalign 0.62
            xalign 0.1
        show sara sorprendida with vpunch:
            xzoom 0.5 yzoom 0.5
            yalign 0.62
            xalign 0.9
        show hipo sorprendido with vpunch:
            xzoom 0.5 yzoom 0.5
            yalign 0.62
            xalign 0.5                       
        syb "¡Es una trampa!"
        syb "¡Nadie sabe donde estamos!"
        syb "¡Nunca nos encontraran!"
        "FIN"
        hide benja
        hide sara
        hide hipo
        jump fin_negativo



label mapa:
    scene islas_chicas
    "El Islote de las 7 pequeñas islas es una zona geográfica perdida y olvidada en el mapa." 
    "Tal como su nombre lo indica, se trata de siete islas conectadas por largos puentes. "
    "Las distancias entre una y otra se pueden recorrer con botes, pero el oleaje es tan alto que se hace complicado trasladarse. "
    "La vida es compleja, todo insume mucho esfuerzo físico. "
    "Las islas se encuentran aisladas y crece poca vegetación, los días son fríos y con mucha ventisca."
    jump mapa_interactivo

label mapa_interactivo:
    call screen mapa
    " "
    jump ver_mapa
label isla_1:
    scene islas at Zoom((1280, 720), (0, 0, 1280, 720), (1008, 1100, 1700, 700), 3)

        #zoom 0.335
        #pause(1)
        #zoom 1
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara "La séptima isla es la más importante a nivel económico. Antiguamente los perros dragones habían permitido que los humanos vivieran en armonía."
    sara"El islote se sostenía en función de un mercado interno de frutas y verduras."
    show iscarotes with vpunch:
        zoom 0.8
        yalign 0.62
        xalign 0.5    
    sara " Pero los Iscarotes, devotos del sol y del fuego destructor, consideraron que lo mejor era extraer el carbón que subyace en el interior de la isla." 
    sara "Para eso desmontaron toda la vegetación, junto a las huertas y jardines. Esto implicó que se destruyeran las fuentes de alimentos y el paisaje también."
    sara "Así el islote de las siete se volvió un lugar inhóspito e insalubre, las nubes de polvo gris llenan el ambiente."
    sara "Los perros dragones tiran de los carros para extraer el carbón."
    sara "En ese sistema esclavista comenzaron las rebeliones: ciertos humanos decidieron que los perros  dragones no podían ser tratados como esclavos y que ese régimen extractivista los estaba destruyendo."
    sara "Finalmente, se impuso el régimen totalitario de los Iscarotes, quienes gobiernan con fuerza de hierro el lugar."
    sara "Los desertores humanos del régimen refugiaron a los sobrevivientes en un oscuro y recóndito paraje "
    sara "del otro lado del Estrecho de las Gárgolas, un espacio entre dos acantilados donde el mar es bravo, ruge,arrastra todo a su paso "
    sara " y destruye las embarcaciones que intentan pasar por ese lugar perdido en el mapa. "
    hide iscarotes
    show hipolito triste with dissolve:
        zoom 0.8
        yalign 0.62
        xalign 0.5      
    sara "Sólo los perros dragones pueden sortear esas inclemencias y refugiarse en cuevas ubicadas en los bordes del acantilado."
    jump mapa_interactivo
label isla_2:
    scene islas at Zoom((1280, 720), (0, 0, 1280, 720), (2100, 800, 1700, 700), 3)
    sara "Es la más pequeña, pero constituye un punto estratégico para ingresar al islote."
    jump mapa_interactivo
label isla_3:
    scene islas at Zoom((1280, 720), (0, 0, 1280, 720), (1900, 400, 1700, 700), 3)
    sara "En esta isla conviven las primeras casas-cuevas realizadas en piedra y las nuevas construcciones."
    jump mapa_interactivo
label isla_4:
    scene islas at Zoom((1280, 720), (0, 0, 1280, 720), (1600, 250, 1700, 700), 3)
    sara "Acà crecen las verduras y frutas que permiten alimentar a la población."
    jump mapa_interactivo
label isla_5:
    scene islas at Zoom((1280, 720), (0, 0, 1280, 720), (400, 300, 1700, 700), 3)
    sara "En esta isla se concentra la mayor parte de la población. "
    jump mapa_interactivo
label isla_6:
    scene islas at Zoom((1280, 720), (0, 0, 1280, 720), (350, 800, 1280, 720), 3)
    sara "Isla volcánica con grandes erupciones. "
    jump mapa_interactivo
label isla_7:
    scene islas at Zoom((1280, 720), (0, 0, 700, 400), (1100, 500, 1500, 700), 3)
    sara "Territorio fértil con grandes cuevas para esconderse."
    show hipolito sentado with wipeleft
    "Hipolito hizo un gruñido"
    menu: 
        "seguir en el mapa":
            jump mapa_interactivo
        "Ver más información":
            "Hipólito pertenece a la raza de los perros dragones que habitan en el Islote de las 7 pequeñas islas."
            "Es hijo de Anaris y nieto de Falkor."
            "Hipólito ladra y aúlla con la voz de todos los ancestros, "
            "y a veces los ojos marrones se le vuelven del color del fuego con destellitos dorados en las pupilas. "
            "Tiene grandes alas que le permiten volar amplias distancias."
    jump mapa_interactivo
label salirmapa:
    scene islas_chicas
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    sara "Ya hemos leido suficiente del mapa"
    jump ver_mapa
label armar_libro:
    scene living 1
    show libro 1:
        yalign 0.6
        xalign 0.5
        zoom 2
    
    benja "Aca dice que si tocamos todos juntos la piedra de Hipolito podemos viajar en el espacio."
    sara "¡Probemos tocar la piedra de Hipólito!."
    show chicos_volando:
        # Show the logo at the upper right side of the screen.
        #xalign 1.0 yalign 0.0

        # Take 1.0 seconds to move things back to the left.
        #linear 1.0 xalign 0.0

        # Take 1.0 seconds to move things to the location specified in the
        # truecenter transform. Use the ease warper to do this.
        ease 1.0 truecenter

        # Just pause for a second.
        #pause 1.0

        # Set the location to circle around.
        alignaround (.5, .5)

        # Use circular motion to bring us to spiral out to the top of
        # the screen. Take 2 seconds to do so.
        linear 2.0 yalign 0.0 clockwise circles 3 zoom 0

        # Use a spline motion to move us around the screen.
        #linear 2.0 align (0.5, 1.0) knot (0.0, .33) knot (1.0, .66)

        # Changes xalign and yalign at thje same time.
        #linear 2.0 xalign 1.0 yalign 1.0
    sara "guauuuuu"
    scene ciudad_isla
    show sara with dissolve:
        yalign 0.62
        xalign 0.9
    show benja with dissolve:
        yalign 0.62
        xalign 0.1
    sara " En este lugar creció Hipolito, nos quedaremos un tiempo con èl."
    jump fin_positivo
##################### FINALES ##########################################
label fin_negativo:
    scene portada2
    "Querido lectojugador, no te preocupes podes continuar explorando otros posibles finales. "
    jump fin
label fin_positivo:
    scene portada1
    "Querido lectojugador has completado una de las posibilidades de la historia. te invitamos a retroceder y elegir otras opciones para tener otro final."
    jump fin
label fin:
